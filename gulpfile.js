// Dependencies

const eslint = require('gulp-eslint'),
      gulp = require('gulp'),
      nodemon = require('gulp-nodemon'),
      notify = require('gulp-notify');

// Linter task
gulp.task('analyze', () => {
  return gulp.src([
    'src/**/*.js',
    '!src/public/js/*.js'
  ])
  .pipe(eslint())
  .pipe(eslint.format())
  .pipe(eslint.failAfterError());
});

// Start dev task
gulp.task('start-dev', () => {
  nodemon({
    script: 'src/app.js',
    ext: 'js',
    ignore: [
      'src/public/js/*.js'
    ],
    env: {
      'NODE_ENV': 'development'
    }
  })
  .on('restart', () => {
    gulp
      .src('src/app.js')
      .pipe(notify('Reloading page, please wait...'));
  });
});

// Default task
gulp.task('default', ['start-dev']);
