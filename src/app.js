'use strict';

/**
 * Modules to use.
 */
const express = require('express'),
      bodyParser = require('body-parser'),
      adminRouter = require('./routes/admin.route'),
      publicRouter = require('./routes/public.route'),
      morgan = require('morgan'),
      flash = require('connect-flash'),
      session = require('express-session'),
      passport = require('passport'),
      cookieParser = require('cookie-parser'),
      validator = require('express-validator'),
      $config = require('./lib/config');

/**
 * Variables to use.
 */
const viewDir = `${__dirname}/views`,
      publicDir = express.static(`${__dirname}/public`),
      port = $config.port || 3000,
      sessionOpts = {
        secret : $config.session.key,
        resave : false,
        saveUninitialized : false,
        cookie: { maxAge: 180 * 60 * 1000 }
      };

// Initialize express.
const app = express();

// Initialize passport
require('./lib/passport');

app.set( 'views', viewDir )
   .set( 'view engine', $config.views.engine )
   .set( 'port', port )

   .use( morgan('dev') )
   .use( bodyParser.json() )
   .use( bodyParser.urlencoded({ extended: false }) )
   .use( validator() )
   .use( cookieParser() )
   .use( session(sessionOpts) )
   .use( flash() )
   .use( passport.initialize() )
   .use( passport.session() )
   .use( '/static', publicDir )
   .use( '/public', publicRouter )
   .use( '/', adminRouter );

if (!module.parent) {
  app.listen(port);
}

module.exports = app;
