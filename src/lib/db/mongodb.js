'use strict';

/**
 * Modules to use.
 */
const mongoose = require('mongoose'),
      $conf = require('../config').database;

mongoose.connect(`mongodb:\/\/${$conf.mongo.host}/${$conf.mongo.db}`);

module.exports = mongoose;
