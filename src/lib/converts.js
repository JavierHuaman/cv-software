'use strict';

// Convert body to work model.
function workbody2model(body) {
  body.generals = body.generals || [];
  body.technologies = body.technologies || [];

  if (typeof body.generals === 'string') {
    body.generals = [body.generals];
  }

  if (typeof body.technologies === 'string') {
    body.technologies = [body.technologies];
  }

  const description = [];

  for (const ii in body.generals) {
    description.push({ general : body.generals[ii], technologies : body.technologies[ii] });
  }

  const work = {
    name : body.name,
    charge : body.charge,
    start : new Date(body.start),
    end : new Date(body.end),
    description : description
  };

  return work;
}

// Convert body to study model.
function studybody2model(body) {
  const study = {
    center : body.center,
    start : new Date(body.start),
    end : new Date(body.end),
    description : body.description
  };

  return study;
}

// Convert body to skill model.
function skillbody2model(body) {
  const skill = {
    header : body.header,
    descriptions : body.descriptions
  };

  return skill;
}

// Convert body to project model.
function projectbody2model(body) {
  const project = {
    name : body.name,
    description : body.description,
    url : body.url,
    technologies : body.technologies
  };

  return project;
}

// Convert body to award model.
function awardbody2model(body) {
  const award = {
    name : body.name,
    center : body.center,
    year : new Date(body.year),
    image : body.image
  };

  return award;
}

// Convert body to reference model.
function referencebody2model(body) {
  const reference = {
    name : body.name,
    url : body.url,
    kind : body.kind,
    icon : body.icon
  };

  return reference;
}

module.exports = {
  workbody2model : workbody2model,
  studybody2model : studybody2model,
  skillbody2model : skillbody2model,
  projectbody2model : projectbody2model,
  awardbody2model : awardbody2model,
  referencebody2model : referencebody2model
};
