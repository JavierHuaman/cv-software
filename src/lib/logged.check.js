'use strict';

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    res.locals.user = req.user;
    return next();
  }
  req.session.oldUrl = req.url;
  return res.redirect('/');
}

function notLoggedIn(req, res, next) {
  if (!req.isAuthenticated()) {
    return next();
  }
  return res.redirect('/');
}

module.exports = {
  isLoggedIn : isLoggedIn,
  notLoggedIn : notLoggedIn
};
