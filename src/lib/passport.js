'use strict';

const passport = require('passport'),
      LocalStrategy = require('passport-local').Strategy,
      userModel = new (require('../models/user.model'))(),
      curriculumModel = new (require('../models/curriculum.model'))(),
      bcrypt = require('bcrypt-nodejs'),
      uuid = require('uuid/v1');

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  userModel.getOne(id, function(err, user) {
    done(err, user);
  });
});

passport.use('local.signup', new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true
}, function(req, email, password, done) {
  req.checkBody('email', 'Correo electronico vacio.').notEmpty().isEmail();
  req.checkBody('dni', 'Número DNI vacio.').notEmpty();
  req.checkBody('name', 'Nombre vacio.').notEmpty();
  req.checkBody('lastname', 'Apellido vacio.').notEmpty();
  req.checkBody('password', 'Contraseña vacio')
    .notEmpty()
    .isLength({ min: 4 });

  const errors = req.validationErrors();
  if (errors) {
    const messages = [];
    errors.forEach(function(error) {
      messages.push(error.msg);
    });
    return done(null, false, req.flash('error', messages));
  }

  userModel.getByDNI(req.body.dni, (user) => {
    if (user) {
      return done(null, false, { message: 'Usuario ya existe con ese DNI.' });
    }
    userModel.getByEmail(email, (err, nuser) => {
      if (err) {
        return done(err);
      }
      if (nuser) {
        return done(null, false, { message: 'Usuario ya existe con ese correo electronico.' });
      }
      const newUser = {
        dni : req.body.dni,
        name : req.body.name,
        lastname : req.body.lastname,
        image : 'emptyimage.png',
        email : email,
        password : bcrypt.hashSync(password, bcrypt.genSaltSync(5), null),
        token : uuid().toString().substring(0, 8)
      };
      userModel.create(newUser, (luser) => {
        const emptyCurriculum = {
          user : luser._id
        };
        curriculumModel.create(emptyCurriculum, () => {
          return done(null, luser);
        });
      });
    });
  });
}));

passport.use('local.signin', new LocalStrategy({
  usernameField : 'email',
  passwordField : 'password',
  passReqToCallback : true
}, (req, email, password, done) => {
  req.checkBody('email', 'Correo electronico incorrecto.').notEmpty().isEmail();
  req.checkBody('password', 'Contraseña incorrecta').notEmpty().isLength({ min : 4 });

  const errors = req.validationErrors();
  if (errors) {
    const messages = [];
    errors.forEach((error) => messages.push(error.msg) );
    return done(null, false, req.flash('error', messages));
  }

  userModel.getByEmail(email, (err, user) => {
    if (err) {
      return done(err);
    }
    if (!user) {
      return done(null, false, { message : 'No se encontro al usuario.' });
    }
    if (!bcrypt.compareSync(password, user.password)) {
      return done(null, false, { message : 'Contraseña incorrecta.' });
    }

    return done(null, user);
  });

}));

