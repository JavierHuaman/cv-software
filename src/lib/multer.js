'use strict';

const multer = require('multer'),
      fs = require('fs-extra'),
      $config = require('../lib/config');

const upload = multer({
  storage: multer.diskStorage({
    destination: (req, file, callback) => {
      const token = req.user.token;
      const path = `${$config.dirUploads.dest}/${token}`;
      fs.mkdirsSync(path);
      callback(null, path);
    }
  })
});

module.exports = upload;
