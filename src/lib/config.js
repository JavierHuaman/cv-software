'use strict';

const developmentConfig = require('../config/development'),
      generalConfig = require('../config/general'),
      productionConfig = require('../config/production');

// Config container
const $config = {
  development : developmentConfig,
  production : productionConfig
};

module.exports = Object.assign(generalConfig, $config[process.env.NODE_ENV]);
