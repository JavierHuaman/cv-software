'use strict';

/**
 * Curriculum instance of Curriculum Schema.
 */
const Curriculum = require('./curriculum.schema');

// Image Model.
class CurriculumModel {

  // Get all curriculums.
  getAll(callback) {
    Curriculum.find({}, (err, curriculums) => {
      if (err) {
        throw err;
      }
      callback(curriculums);
    });
  }

  // Get curriculum by id.
  getOne(_id, callback) {
    Curriculum.findOne({ _id : _id }, (err, curriculum) => {
      if (err) {
        throw err;
      }
      callback(curriculum);
    });
  }

  // Get curriculum by user.
  getByUser(_userId, callback) {
    Curriculum.findOne({ user : _userId }, (err, curriculum) => {
      if (err) {
        throw err;
      }
      callback(curriculum);
    });
  }

  // Create new curriculum.
  create(data, callback) {
    Curriculum.create(data, (err, curriculum) => {
      if (err) {
        throw err;
      }
      callback();
    });
  }

  // Update curriculum.
  update(_id, data, callback) {
    Curriculum.findOneAndUpdate({ _id : _id }, data, (err) => {
      if (err) {
        throw err;
      }
      callback();
    });
  }

  // Delete curriculum.
  delete(_id, callback) {
    Curriculum.remove({ _id: _id }, (err) => {
      if (err) {
        throw err;
      }
      callback();
    });
  }

}

module.exports = CurriculumModel;
