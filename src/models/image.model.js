'use strict';

/**
 * Image instance of Image Schema.
 */
const Image = require('./image.schema');

// Image Model.
class ImageModel {

  // Get all images.
  getAll(callback) {
    Image.find({}, (err, images) => {
      if (err) {
        throw err;
      }
      callback(images);
    });
  }

  // Get image by id.
  getOne(_id, callback) {
    Image.findOne({ _id : _id }, (err, image) => {
      if (err) {
        throw err;
      }
      callback(image);
    });
  }

  // Get images by type
  getAllUserImages(_userId, callback) {
    Image.find({ type : 'imageprofile', user : _userId }, (err, images) => {
      if (err) {
        throw err;
      }
      callback(images);
    });
  }

  // Create new image.
  create(data, callback) {
    Image.create(data, (err, image) => {
      if (err) {
        throw err;
      }
      callback();
    });
  }

  // Update image.
  update(_id, data, callback) {
    Image.findOneAndUpdate({ _id : _id }, data, (err) => {
      if (err) {
        throw err;
      }
      callback();
    });
  }

  // Delete image.
  delete(_id, callback) {
    Image.remove({ _id: _id }, (err) => {
      if (err) {
        throw err;
      }
      callback();
    });
  }

}

module.exports = ImageModel;
