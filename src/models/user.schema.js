'use strict';

/**
 * Modules to use.
 */
const mongoose = require('../lib/db/mongodb'),
      Schema = mongoose.Schema;

/**
 * User Schema
 */
const userSchema = new Schema({
  // 'Numero de DNI'
  dni : String,
  // 'Nombres'
  name : String,
  // 'Apellidos'
  lastname : String,
  // 'Fecha Nacimiento'
  birthday : Date,
  // 'Direccion'
  address : String,
  // 'Celular'
  cellphone : String,
  // 'Correo'
  email : String,
  // 'Descripcion personal'
  description : String,
  // 'Cargo'
  charge : String,
  // 'Imagen perfil'
  image : String,
  // 'Token'
  token : String,
  // 'Password'
  password : String
});

module.exports = mongoose.model('User', userSchema);
