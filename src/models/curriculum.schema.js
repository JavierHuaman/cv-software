'use strict';

/**
 * Modules to use.
 */
const mongoose = require('../lib/db/mongodb'),
      Schema = mongoose.Schema,
      ObjectId = Schema.ObjectId;

/**
 * Work Schema.
 * 'Experiencia Laboral Esquema.'
 */
const WorkSchema = {
  // 'Razon social'
  name : String,
  // 'Fecha inicio'
  start : Date,
  // 'Fecha fin'
  end : Date,
  // 'Cargo'
  charge : String,
  // 'Descripcion'
  description : [{
    // 'General'
    general : String,
    // 'Tecnologia'
    technologies : String
  }]
};

/**
 * Study Schema.
 * 'Estudios Realizados Esquema.'
 */
const StudySchema = {
  // 'Centro de Estudios'
  center : String,
  // 'Fecha inicio'
  start : Date,
  // 'Fecha fin'
  end : Date,
  // 'Descripción'
  description : String
};

/**
 * Skill Schema.
 * 'Conocimientos Esquema.'
 */
const SkillSchema = {
  // 'Cabecera'
  header : String,
  // 'Descripciones.'
  descriptions : String
};

/**
 * Project Schema.
 * 'Proyectos Esquema.'
 */
const ProjectSchema = {
  // 'Nombre'
  name : String,
  // 'Descripción'
  description : String,
  // 'URL'
  url : String,
  // 'Tecnologias'
  technologies : String
};

/**
 * Award Schema.
 * 'Cursos/Seminario/Constancia Esquema.'
 */
const AwardSchema = {
  // 'Nombre'
  name : String,
  // 'Institución'
  center : String,
  // 'Año Realizado'
  year : Date,
  // 'Imagen'
  image : String
};

/**
 * Reference Schema.
 * 'Referencias/Perfil público  Esquema.'
 */
const ReferenceSchema = {
  // 'Nombre'
  name : String,
  // 'URL'
  url : String,
  // 'Tipo'
  kind : String,
  // 'Icon'
  icon : String
};

/**
 * Curriculum Schema.
 * 'Curriculum vitae Esquema.'
 */
const CurriculumSchema = new Schema({
  // 'User DNI'
  user : {
    type : ObjectId,
    ref : 'User'
  },
  // 'Experiencia Laboral'
  works : [WorkSchema],
  // 'Estudios realizados'
  studies : [StudySchema],
  // 'Conocimientos Informatica'
  skills : [SkillSchema],
  // 'Proyectos'
  projects : [ProjectSchema],
  // 'Cursos/Seminarios/Constancias'
  awards : [AwardSchema],
  // 'Referencias'
  references : [ReferenceSchema]
});

module.exports = mongoose.model('Curriculum', CurriculumSchema);
