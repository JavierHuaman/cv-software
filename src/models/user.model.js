'use strict';

/**
 * User instance of User Schema.
 */
const User = require('./user.schema');

/**
 * User Model.
 */
class UserModel {

  // Get all users.
  getAll(callback) {
    User.find({}, (err, users) => {
      if (err) {
        throw err;
      }
      callback(users);
    });
  }

  // Get user by id.
  getOne(_id, callback) {
    User.findOne({ _id : _id }, (err, user) => {
      if (err) {
        throw err;
      }
      callback(err, user);
    });
  }

  // Get user by id.
  getByDNI(dni, callback) {
    User.findOne({ dni : dni }, (err, user) => {
      if (err) {
        throw err;
      }
      callback(user);
    });
  }

  // Get user by token.
  getByToken(token, callback) {
    User.findOne({ token : token }, (err, user) => {
      if (err) {
        throw err;
      }
      callback(user);
    });
  }

  // Create new user.
  create(data, callback) {
    User.create(data, (err, user) => {
      if (err) {
        throw err;
      }
      callback(user);
    });
  }

  // Update user.
  update(_id, data, callback) {
    User.findOneAndUpdate({ _id : _id }, data, (err) => {
      if (err) {
        throw err;
      }
      callback();
    });
  }

  // Delete user.
  delete(_id, callback) {
    User.remove({ _id: _id }, (err) => {
      if (err) {
        throw err;
      }
      callback();
    });
  }

  // Authenticate user.
  getByEmail(email, callback) {
    User.findOne({ email : email }, (err, user) => {
      callback(err, user);
    });
  }

}

module.exports = UserModel;
