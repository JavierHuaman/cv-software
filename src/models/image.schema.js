'use strict';

/**
 * Modules to use.
 */
const mongoose = require('../lib/db/mongodb'),
      Schema = mongoose.Schema,
      ObjectId = Schema.ObjectId;

/**
 * Image Schema
 */
const ImageSchema = new Schema({
  id : ObjectId,
  // 'Nombre'
  name : String,
  // 'Tipo'
  type : String,
  // 'User DNI'
  user : {
    type : ObjectId,
    ref : 'User'
  }
});

module.exports = mongoose.model('Image', ImageSchema);
