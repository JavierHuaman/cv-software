'use strict';

const curriculumModel = new (require('../models/curriculum.model'))(),
      converts = require('../lib/converts');

class SkillController {

  index(req, res, next) {
    const userId = req.user.id;
    const message = req.flash('message')[0];
    curriculumModel.getByUser(userId, (curriculum) => {
      res.render('admin/skill', { skills : curriculum.skills, message : message, title: 'Conocimiento Informatico'});
    });
  }

  create(req, res, next) {
    const userId = req.user.id;
    const skill = converts.skillbody2model(req.body);

    curriculumModel.getByUser(userId, (curriculum) => {
      const currIndex = curriculum.skills.findIndex(ss => ss.header === skill.header);
      if (currIndex === -1) {
        curriculum.skills.push(skill);

        curriculumModel.update(curriculum._id, curriculum, () => {
          req.flash('message', {
            text : 'Se guardo satisfactoriamente.',
            class : 'success'
          });
          res.redirect('/skill');
        });
      } else {
        req.flash('message', {
          text : 'Ya existe Conocimiento Informatico.',
          class : 'error'
        });
        res.redirect('/skill');
      }
    });
  }

  update(req, res, next) {
    const userId = req.user.id;
    const skill = converts.skillbody2model(req.body);
    const skillId = req.params.id.toString();

    curriculumModel.getByUser(userId, (curriculum) => {
      const currIndex = curriculum.skills.findIndex(ss => ss.id === skillId);
      if (currIndex === -1) {
        req.flash('message', {
          text : 'No existe Conocimiento Informatico',
          class : 'error'
        });
        res.redirect('/skill');
      } else {
        curriculum.skills[currIndex] = skill;

        curriculumModel.update(curriculum._id, curriculum, () => {
          req.flash('message', {
            text : 'Se actualizo satisfactoriamente.',
            class : 'success'
          });
          res.redirect('/skill');
        });
      }
    });
  }

  delete(req, res, next) {
    const userId = req.user.id;
    const skillId = req.params.id.toString();

    curriculumModel.getByUser(userId, (curriculum) => {
      const skills = curriculum.skills.filter(ss => ss.id !== skillId);
      curriculum.skills = skills;
      curriculumModel.update(curriculum.id, curriculum, () => {
        req.flash('message', {
          text : 'Se elimino satisfactoriamente.',
          class : 'success'
        });
        res.redirect('/skill');
      });
    });
  }

}

module.exports = SkillController;
