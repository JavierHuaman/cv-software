'use strict';

const userModel = new (require('../models/user.model'))(),
      imageModel = new (require('../models/image.model'))(),
      bcrypt = require('bcrypt-nodejs');

class AuthController {

  signinPage(req, res, next) {
    const messages = req.flash('error');
    res.render('admin/signin', { messages : messages });
  }

  signupPage(req, res, next) {
    const messages = req.flash('error');
    res.render('admin/signup', { messages : messages });
  }

  profilePage(req, res, next) {
    const userId = req.user.id;
    const message = req.flash('message')[0];
    userModel.getOne(userId, (err, user) => {
      if (err) {
        throw err;
      }

      imageModel.getAllUserImages(user._id, (images) => {
        res.render(
          'admin/profile',
          { currentUser : user,
            message : message,
            title: `Perfil :  ${user.name} ${user.lastname}`,
            allphotos : images }
        );
      });
    });
  }

  profileSave(req, res, next) {
    const user = {
      dni : req.body.dni,
      name : req.body.name,
      lastname : req.body.lastname,
      birthday : new Date(req.body.birthday),
      address : req.body.address,
      cellphone : req.body.cellphone,
      description : req.body.description,
      charge : req.body.charge
    };

    userModel.getByDNI(user.dni, (tosave) => {
      Object.assign(tosave, user);
      userModel.update(tosave._id, tosave, () => {
        req.flash('message', {
          text : 'Se guardo satisfactoriamente.',
          class : 'success'
        });
        res.redirect('/profile');
      });
    });
  }

  changePassword(req, res, next) {
    const userId = req.user.id;
    userModel.getOne(userId, (err, user) => {
      if (err) {
        throw err;
      }
      if (!bcrypt.compareSync(req.body.currentpassword, user.password)) {
        req.flash('message', {
          text : 'La contraseña no existe en la base de datos.',
          class : 'negative'
        });
        res.redirect('/profile');
      } else {
        user.password = bcrypt.hashSync(req.body.newpassword, bcrypt.genSaltSync(5), null);
        userModel.update(user._id, user, () => {
          req.flash('message', {
            text : 'Se ha actualizado la contraseña.',
            class : 'success'
          });
          res.redirect('/profile');
        });
      }
    });
  }

  changeImageProfile(req, res, next) {
    const user = req.user;
    let titleimage = `${user.token}/${req.body.title}`;
    if (req.file) {
      titleimage = `${user.token}/${req.file.filename}`;
      const image = {
        name : titleimage,
        type : 'imageprofile',
        user : user._id
      };
      imageModel.create(image, () => {
        // Save image
      });
    }
    user.image = titleimage;
    userModel.update(user._id, user, () => {
      req.flash('message', {
        text : 'Se ha actualizado la foto de perfil.',
        class : 'success'
      });
      res.redirect('/profile');
    });
  }

  signin(req, res, next) {
    if (req.session.oldUrl) {
      const oldUrl = req.session.oldUrl;
      req.session.oldUrl = null;
      res.redirect(oldUrl);
    } else {
      res.redirect('/work');
    }
  }

  signup(req, res, next) {
    if (req.session.oldUrl) {
      const oldUrl = req.session.oldUrl;
      req.session.oldUrl = null;
      res.redirect(oldUrl);
    } else {
      res.redirect('/profile');
    }
  }

  logout(req, res, next) {
    req.logout();
    res.redirect('/');
  }

}

module.exports = AuthController;
