'use strict';

const curriculumModel = new (require('../models/curriculum.model'))(),
      converts = require('../lib/converts');

class ReferenceController {

  index(req, res, next) {
    const userId = req.user.id;
    const message = req.flash('message')[0];
    curriculumModel.getByUser(userId, (curriculum) => {
      res.render('admin/reference',
                 { references : curriculum.references, message : message, title: 'Referencias (perfiles).' }
      );
    });
  }

  create(req, res, next) {
    const userId = req.user.id;
    const reference = converts.referencebody2model(req.body);

    curriculumModel.getByUser(userId, (curriculum) => {
      const currIndex = curriculum.references.findIndex(ss => ss.name === reference.name);
      if (currIndex === -1) {
        curriculum.references.push(reference);

        curriculumModel.update(curriculum._id, curriculum, () => {
          req.flash('message', {
            text : 'Se guardo satisfactoriamente.',
            class : 'success'
          });
          res.redirect('/reference');
        });
      } else {
        req.flash('message', {
          text : 'Ya existe Referencia.',
          class : 'error'
        });
        res.redirect('/reference');
      }
    });
  }

  update(req, res, next) {
    const userId = req.user.id;
    const reference = converts.referencebody2model(req.body);
    const referenceId = req.params.id.toString();

    curriculumModel.getByUser(userId, (curriculum) => {
      const currIndex = curriculum.references.findIndex(ss => ss.id === referenceId);
      if (currIndex === -1) {
        req.flash('message', {
          text : 'No existe Referencia.',
          class : 'error'
        });
        res.redirect('/reference');
      } else {
        curriculum.references[currIndex] = reference;

        curriculumModel.update(curriculum._id, curriculum, () => {
          req.flash('message', {
            text : 'Se actualizo satisfactoriamente.',
            class : 'success'
          });
          res.redirect('/reference');
        });
      }
    });
  }

  delete(req, res, next) {
    const userId = req.user.id;
    const referenceId = req.params.id.toString();

    curriculumModel.getByUser(userId, (curriculum) => {
      const references = curriculum.references.filter(ss => ss.id !== referenceId);
      curriculum.references = references;
      curriculumModel.update(curriculum.id, curriculum, () => {
        req.flash('message', {
          text : 'Se elimino satisfactoriamente.',
          class : 'success'
        });
        res.redirect('/reference');
      });
    });
  }

}

module.exports = ReferenceController;
