'use strict';

const curriculumModel = new (require('../models/curriculum.model'))(),
      converts = require('../lib/converts');

class AwardController {

  index(req, res, next) {
    const userId = req.user.id;
    const message = req.flash('message')[0];
    curriculumModel.getByUser(userId, (curriculum) => {
      res.render('admin/award',
                 { awards : curriculum.awards, message : message, title: 'Cursos/Seminarios/Constancias' });
    });
  }

  create(req, res, next) {
    const userId = req.user.id;
    const award = converts.awardbody2model(req.body);

    curriculumModel.getByUser(userId, (curriculum) => {
      const currIndex = curriculum.awards.findIndex(ss => ss.name === award.name);
      if (currIndex === -1) {
        curriculum.awards.push(award);

        curriculumModel.update(curriculum._id, curriculum, () => {
          req.flash('message', {
            text : 'Se guardo satisfactoriamente.',
            class : 'success'
          });
          res.redirect('/award');
        });
      } else {
        req.flash('message', {
          text : 'Ya existe Cursos/Seminarios/Constancias.',
          class : 'error'
        });
        res.redirect('/award');
      }
    });
  }

  update(req, res, next) {
    const userId = req.user.id;
    const award = converts.awardbody2model(req.body);
    const awardId = req.params.id.toString();

    curriculumModel.getByUser(userId, (curriculum) => {
      const currIndex = curriculum.awards.findIndex(ss => ss.id === awardId);
      if (currIndex === -1) {
        req.flash('message', {
          text : 'No existe Cursos/Seminarios/Constancias.',
          class : 'error'
        });
        res.redirect('/award');
      } else {
        curriculum.awards[currIndex] = award;

        curriculumModel.update(curriculum._id, curriculum, () => {
          req.flash('message', {
            text : 'Se actualizo satisfactoriamente.',
            class : 'success'
          });
          res.redirect('/award');
        });
      }
    });
  }

  delete(req, res, next) {
    const userId = req.user.id;
    const awardId = req.params.id.toString();

    curriculumModel.getByUser(userId, (curriculum) => {
      const awards = curriculum.awards.filter(ss => ss.id !== awardId);
      curriculum.awards = awards;
      curriculumModel.update(curriculum.id, curriculum, () => {
        req.flash('message', {
          text : 'Se elimino satisfactoriamente.',
          class : 'success'
        });
        res.redirect('/award');
      });
    });
  }

}

module.exports = AwardController;
