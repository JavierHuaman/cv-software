'use strict';

const curriculumModel = new (require('../models/curriculum.model'))(),
      converts = require('../lib/converts');

class ProjectController {

  index(req, res, next) {
    const userId = req.user.id;
    const message = req.flash('message')[0];
    curriculumModel.getByUser(userId, (curriculum) => {
      res.render('admin/project', { projects : curriculum.projects, message : message, title: 'Proyectos Personales'});
    });
  }

  create(req, res, next) {
    const userId = req.user.id;
    const project = converts.projectbody2model(req.body);

    curriculumModel.getByUser(userId, (curriculum) => {
      const currIndex = curriculum.projects.findIndex(ss => ss.name === project.name);
      if (currIndex === -1) {
        curriculum.projects.push(project);

        curriculumModel.update(curriculum._id, curriculum, () => {
          req.flash('message', {
            text : 'Se guardo satisfactoriamente.',
            class : 'success'
          });
          res.redirect('/project');
        });
      } else {
        req.flash('message', {
          text : 'Ya existe Conocimiento Informatico.',
          class : 'error'
        });
        res.redirect('/project');
      }
    });
  }

  update(req, res, next) {
    const userId = req.user.id;
    const project = converts.projectbody2model(req.body);
    const projectId = req.params.id.toString();

    curriculumModel.getByUser(userId, (curriculum) => {
      const currIndex = curriculum.projects.findIndex(ss => ss.id === projectId);
      if (currIndex === -1) {
        req.flash('message', {
          text : 'No existe Conocimiento Informatico',
          class : 'error'
        });
        res.redirect('/project');
      } else {
        curriculum.projects[currIndex] = project;

        curriculumModel.update(curriculum._id, curriculum, () => {
          req.flash('message', {
            text : 'Se actualizo satisfactoriamente.',
            class : 'success'
          });
          res.redirect('/project');
        });
      }
    });
  }

  delete(req, res, next) {
    const userId = req.user.id;
    const projectId = req.params.id.toString();

    curriculumModel.getByUser(userId, (curriculum) => {
      const projects = curriculum.projects.filter(ss => ss.id !== projectId);
      curriculum.projects = projects;
      curriculumModel.update(curriculum.id, curriculum, () => {
        req.flash('message', {
          text : 'Se elimino satisfactoriamente.',
          class : 'success'
        });
        res.redirect('/project');
      });
    });
  }

}

module.exports = ProjectController;
