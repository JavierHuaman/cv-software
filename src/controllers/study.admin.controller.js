'use strict';

const curriculumModel = new (require('../models/curriculum.model'))(),
      converts = require('../lib/converts');

class StudyController {

  index(req, res, next) {
    const userId = req.user.id;
    const message = req.flash('message')[0];
    curriculumModel.getByUser(userId, (curriculum) => {
      res.render('admin/study', { studies : curriculum.studies, message : message, title: 'Estudios Realizados'});
    });
  }

  create(req, res, next) {
    const userId = req.user.id;
    const study = converts.studybody2model(req.body);

    curriculumModel.getByUser(userId, (curriculum) => {
      const currIndex = curriculum.studies.findIndex(ss => ss.center === study.center);
      if (currIndex === -1) {
        curriculum.studies.push(study);

        curriculumModel.update(curriculum._id, curriculum, () => {
          req.flash('message', {
            text : 'Se guardo satisfactoriamente.',
            class : 'success'
          });
          res.redirect('/study');
        });
      } else {
        req.flash('message', {
          text : 'Ya existe estudios',
          class : 'error'
        });
        res.redirect('/study');
      }
    });
  }

  update(req, res, next) {
    const userId = req.user.id;
    const study = converts.studybody2model(req.body);
    const center = req.params.center;

    curriculumModel.getByUser(userId, (curriculum) => {
      const currIndex = curriculum.studies.findIndex(ss => ss.center === center);
      if (currIndex === -1) {
        req.flash('message', {
          text : 'No existe trabajo',
          class : 'error'
        });
        res.redirect('/study');
      } else {
        curriculum.studies[currIndex] = study;

        curriculumModel.update(curriculum._id, curriculum, () => {
          req.flash('message', {
            text : 'Se actualizo satisfactoriamente.',
            class : 'success'
          });
          res.redirect('/study');
        });
      }
    });
  }

  delete(req, res, next) {
    const userId = req.user.id;
    const center = req.params.center;

    curriculumModel.getByUser(userId, (curriculum) => {
      const studies = curriculum.studies.filter(ss => ss.center !== center);
      curriculum.studies = studies;
      curriculumModel.update(curriculum.id, curriculum, () => {
        req.flash('message', {
          text : 'Se elimino satisfactoriamente.',
          class : 'success'
        });
        res.redirect('/study');
      });
    });
  }

}

module.exports = StudyController;
