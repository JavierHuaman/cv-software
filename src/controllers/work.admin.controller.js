'use strict';

const curriculumModel = new (require('../models/curriculum.model'))(),
      converts = require('../lib/converts');

class WorkController {

  index(req, res, next) {
    const userId = req.user.id;
    const message = req.flash('message')[0];
    curriculumModel.getByUser(userId, (curriculum) => {
      res.render('admin/work', { works : curriculum.works, message : message, title: 'Experiencia Laboral'});
    });
  }

  create(req, res, next) {
    const userId = req.user.id;
    const work = converts.workbody2model(req.body);

    curriculumModel.getByUser(userId, (curriculum) => {
      const currIndex = curriculum.works.findIndex(ww => ww.name === work.name);
      if (currIndex === -1) {
        curriculum.works.push(work);

        curriculumModel.update(curriculum._id, curriculum, () => {
          req.flash('message', {
            text : 'Se guardo satisfactoriamente.',
            class : 'success'
          });
          res.redirect('/work');
        });
      } else {
        req.flash('message', {
          text : 'Ya existe trabajo',
          class : 'error'
        });
        res.redirect('/work');
      }
    });
  }

  update(req, res, next) {
    const userId = req.user.id;
    const work = converts.workbody2model(req.body);
    const realName = req.params.name;

    curriculumModel.getByUser(userId, (curriculum) => {
      const currIndex = curriculum.works.findIndex(ww => ww.name === realName);
      if (currIndex === -1) {
        req.flash('message', {
          text : 'No existe trabajo',
          class : 'error'
        });
        res.redirect('/work');
      } else {
        curriculum.works[currIndex] = work;

        curriculumModel.update(curriculum._id, curriculum, () => {
          req.flash('message', {
            text : 'Se actualizo satisfactoriamente.',
            class : 'success'
          });
          res.redirect('/work');
        });
      }
    });
  }

  delete(req, res, next) {
    const userId = req.user.id;
    const realName = req.params.name;

    curriculumModel.getByUser(userId, (curriculum) => {
      const works = curriculum.works.filter(ww => ww.name !== realName);
      curriculum.works = works;
      curriculumModel.update(curriculum.id, curriculum, () => {
        req.flash('message', {
          text : 'Se elimino satisfactoriamente.',
          class : 'success'
        });
        res.redirect('/work');
      });
    });
  }

}

module.exports = WorkController;
