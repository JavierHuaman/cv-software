'use strict';

const UserSchema = new require('../models/user.schema'),
      CurriculumSchema = new require('../models/curriculum.schema'),
      bcrypt = require('bcrypt-nodejs');

const user = {
  dni : '70236561',
  name : 'Javier Antonio',
  lastname : 'Huaman Adama',
  email : 'javier.adama@gmail.com',
  password : bcrypt.hashSync('123456', bcrypt.genSaltSync(5), null)
};

UserSchema.create(user, (err, user) => {
  if (err) {
    throw err;
  }
  const curriculum = {
    user : user._id
  };
  CurriculumSchema.create(curriculum, (err1, curr) => {
    if (err1) {
      throw err1;
    }
  });
});
