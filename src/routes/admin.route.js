'use strict';

const router = require('express').Router(),
      passport = require('passport'),
      workController = new (require('../controllers/work.admin.controller'))(),
      studyController = new (require('../controllers/study.admin.controller'))(),
      skillController = new (require('../controllers/skill.admin.controller'))(),
      projectController = new (require('../controllers/project.admin.controller'))(),
      awardController = new (require('../controllers/award.admin.controller'))(),
      referenceController = new (require('../controllers/reference.admin.controller'))(),
      authController = new (require('../controllers/auth.admin.controller'))(),
      loggedCheck = require('../lib/logged.check'),
      upload = require('../lib/multer');

// Index page
router.get('/', authController.signinPage);

// Login page
router.post('/signin', passport.authenticate('local.signin', {
  failureRedirect : '/',
  failureFlash : true
}), authController.signin);

// Signup page
router.get('/signup', authController.signupPage);

// Signup
router.post('/signup', passport.authenticate('local.signup', {
  failureRedirect : '/signup',
  failureFlash : true
}), authController.signup);

// Logout page
router.get('/logout', loggedCheck.isLoggedIn, authController.logout);

// Profile page
router.get('/profile', loggedCheck.isLoggedIn, authController.profilePage);
router.post('/profile', loggedCheck.isLoggedIn, authController.profileSave);
router.post('/changepassword', loggedCheck.isLoggedIn, authController.changePassword);
router.post('/changeimageprofile', loggedCheck.isLoggedIn,
            upload.single('imageprofile'), authController.changeImageProfile);

// Work page
router.get('/work', loggedCheck.isLoggedIn, workController.index);
router.post('/work/create', loggedCheck.isLoggedIn, workController.create);
router.post('/work/:name/update', loggedCheck.isLoggedIn, workController.update);
router.post('/work/:name/delete', loggedCheck.isLoggedIn, workController.delete);

// Study page
router.get('/study', loggedCheck.isLoggedIn, studyController.index);
router.post('/study/create', loggedCheck.isLoggedIn, studyController.create);
router.post('/study/:center/update', loggedCheck.isLoggedIn, studyController.update);
router.post('/study/:center/delete', loggedCheck.isLoggedIn, studyController.delete);

// Skill page
router.get('/skill', loggedCheck.isLoggedIn, skillController.index);
router.post('/skill/create', loggedCheck.isLoggedIn, skillController.create);
router.post('/skill/:id/update', loggedCheck.isLoggedIn, skillController.update);
router.post('/skill/:id/delete', loggedCheck.isLoggedIn, skillController.delete);

// Project page
router.get('/project', loggedCheck.isLoggedIn, projectController.index);
router.post('/project/create', loggedCheck.isLoggedIn, projectController.create);
router.post('/project/:id/update', loggedCheck.isLoggedIn, projectController.update);
router.post('/project/:id/delete', loggedCheck.isLoggedIn, projectController.delete);

// Award page
router.get('/award', loggedCheck.isLoggedIn, awardController.index);
router.post('/award/create', loggedCheck.isLoggedIn, awardController.create);
router.post('/award/:id/update', loggedCheck.isLoggedIn, awardController.update);
router.post('/award/:id/delete', loggedCheck.isLoggedIn, awardController.delete);

// Reference page
router.get('/reference', loggedCheck.isLoggedIn, referenceController.index);
router.post('/reference/create', loggedCheck.isLoggedIn, referenceController.create);
router.post('/reference/:id/update', loggedCheck.isLoggedIn, referenceController.update);
router.post('/reference/:id/delete', loggedCheck.isLoggedIn, referenceController.delete);

module.exports = router;
