'use strict';

const router = require('express').Router(),
      curriculumModel = new (require('../models/curriculum.model'))(),
      userModel = new (require('../models/user.model'))();

router.get('/cv/:token', (req, res) => {
  userModel.getByToken(req.params.token, (user) => {
    curriculumModel.getByUser(user._id, (curriculum) => {
      curriculum.user = user;
      res.render('public/index', { curriculum : curriculum });
    });
  });
});

module.exports = router;
