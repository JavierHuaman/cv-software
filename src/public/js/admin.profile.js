// General
$('#profile-tab .item').tab();

////////////////////////////////////////////////
// Tab profile.                               //
////////////////////////////////////////////////
var $btnEdit = $('#btnEdit'),
    $btnCancel = $('#btnCancel'),
    $btnSave = $('#btnSave'),
    $modalImageProfile = $('#modal-image-profile'),
    $frmNewPhoto = $modalImageProfile.find('#frm-new-photo'),
    $imgInputFile = $frmNewPhoto.find('#img-input-file'),
    $imgUploaded = $frmNewPhoto.find('#img-uploaded'),
    $imgTitle = $frmNewPhoto.find('#img-title'),
    $btnChangeImage = $('#btn-change-image'),
    $frmGeneral = $('#frm-general'),
    $selectImage = $('.select-image'),
    birthday = $frmGeneral.find('[name=birthday]');

// TODO (replace date)
birthday.val(birthday.val().replace('T00:00:00.000Z', ''))

$btnEdit.click(function() {
  $(this).hide();
  $btnSave.show();
  $btnCancel.show();
  $frmGeneral.find('.text-value').removeAttr('readonly');
});

$btnCancel.click(function() {
  $btnEdit.show();
  $btnSave.hide();
  $btnCancel.hide();
  $frmGeneral.find('.text-value').attr('readonly', true);
});

$modalImageProfile
  .modal({
    onApprove : function() {
      $frmNewPhoto.submit();
    },
    onHidden : function() {
      $frmNewPhoto[0].reset();
      $imgUploaded
        .attr('src', '#')
        .parent().hide();
      $imgInputFile
        .parent().show();
    },
    observeChanges : true
  })
  .modal('attach events', ('#' + $btnChangeImage.attr('id')) , 'show');

$modalImageProfile.find('#tab-photos .item').tab();

function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $imgUploaded
        .attr('src', e.target.result)
        .parent().show();
      $imgInputFile
        .parent().hide();
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$imgInputFile.change(function() {
  $('.selected-image').removeClass('selected-image');
  $imgTitle.val('');
  readURL(this);
});

$selectImage.click(function() {
  $('.selected-image').removeClass('selected-image');
  $(this).parent().addClass('selected-image');
  $frmNewPhoto[0].reset();
  $imgUploaded
    .attr('src', '#')
    .parent().hide();
  $imgInputFile
    .parent().show();
  $imgTitle.val($(this).attr('data-name'));
});

////////////////////////////////////////////////
// End Tab profile.                           //
////////////////////////////////////////////////

////////////////////////////////////////////////
// Tab changepassword.                        //
////////////////////////////////////////////////
var $frmChangePassword = $('#frm-changepassword');


////////////////////////////////////////////////
// End Tab changepassword.                    //
////////////////////////////////////////////////
