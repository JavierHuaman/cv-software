
var $projects = JSON.parse(document.querySelector('[name=projects]').value),
    $projectsContainer = $('#projects-container'),
    $frmAddProject = $('#frm-add-project'),
    $frmRemoveProject = $('#frm-remove-project'),
    $modalAddProject = $('#modal-add-project'),
    $modalRemoveProject = $('#modal-remove-project'),
    $btnAddProject = $('#btn-add-project');

$modalAddProject
  .modal({
    onApprove : function() {
      $frmAddProject.submit();
      return false;
    },
    observeChanges : true
  })
  .modal('attach events', ('#' + $btnAddProject.attr('id')) , 'show');

$modalRemoveProject
  .modal({
    onApprove : function() {
      $frmRemoveProject.submit();
    }
  })
  .modal('attach events', '#projects-container .remove-project' , 'show');

$btnAddProject.click(function() {
  $frmAddProject.form('reset');
  $frmAddProject.form('clear');
  $frmAddProject.attr('action', '/project/create');
  $modalAddProject.find('.header').html('Agregar Estudios Realizados');
});

$projectsContainer.find('.edit-project').click(function() {
  $frmAddProject.form('reset');
  $frmAddProject.form('clear');
  var id = $(this).attr('data-id');
  var currentProject = $projects.find( function(project) { return project._id == id } );

  $frmAddProject.find('[name=name]').val(currentProject.name);
  $frmAddProject.find('[name=url]').val(currentProject.url);
  $frmAddProject.find('[name=description]').html(currentProject.description);
  $frmAddProject.find('[name=technologies]').html(currentProject.technologies);
  $frmAddProject.attr('action', '/project/' + id + '/update');
  $modalAddProject.find('.header').html('Editar Cursos: ' + currentProject.name );
  $modalAddProject.modal('show');
});

$projectsContainer.find('.remove-project').click(function() {
  var id = $(this).attr('data-id');
  var currentProject = $projects.find( function(project) { return project._id == id } );

  $modalRemoveProject.find('.header').html('Desea eliminar el cursos/serminario ' + currentProject.name + '?');
  $frmRemoveProject.attr('action', '/project/' + id + '/delete');
});

$frmAddProject.form({
  inline: true,
  fields: {
    name: {
      identifier: 'name',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese el nombre'
        }
      ]
    },
    url: {
      identifier: 'url',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese la URL.'
        }
      ]
    },
    description: {
      identifier: 'description',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese la descripción.'
        }
      ]
    },
    technologies: {
      identifier: 'technologies',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese las tecnologias usadas.'
        }
      ]
    }
  }
});
