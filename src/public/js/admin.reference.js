
var $references = JSON.parse(document.querySelector('[name=references]').value),
    $referencesContainer = $('#references-container'),
    $frmAddReference = $('#frm-add-reference'),
    $frmRemoveReference = $('#frm-remove-reference'),
    $modalAddReference = $('#modal-add-reference'),
    $modalRemoveReference = $('#modal-remove-reference'),
    $btnAddReference = $('#btn-add-reference');

$modalAddReference
  .modal({
    onApprove : function() {
      $frmAddReference.submit();
      return false;
    },
    observeChanges : true
  })
  .modal('attach events', ('#' + $btnAddReference.attr('id')) , 'show');

$modalRemoveReference
  .modal({
    onApprove : function() {
      $frmRemoveReference.submit();
    }
  })
  .modal('attach events', '#references-container .remove-reference' , 'show');

$btnAddReference.click(function() {
  $frmAddReference.form('reset');
  $frmAddReference.form('clear');
  $frmAddReference.attr('action', '/reference/create');
  $modalAddReference.find('.header').html('Agregar Estudios Realizados');
});

$referencesContainer.find('.edit-reference').click(function() {
  $frmAddReference.form('reset');
  $frmAddReference.form('clear');
  var id = $(this).attr('data-id');
  var currentReference = $references.find( function(reference) { return reference._id == id } );

  $frmAddReference.find('[name=name]').val(currentReference.name);
  $frmAddReference.find('[name=url]').val(currentReference.url);
  $frmAddReference.find('[name=kind]').val(currentReference.kind);
  $frmAddReference.find('[name=icon]').val(currentReference.icon);
  $frmAddReference.attr('action', '/reference/' + id + '/update');
  $modalAddReference.find('.header').html('Editar Cursos: ' + currentReference.name );
  $modalAddReference.modal('show');
});

$referencesContainer.find('.remove-reference').click(function() {
  var id = $(this).attr('data-id');
  var currentReference = $references.find( function(reference) { return reference._id == id } );

  $modalRemoveReference.find('.header').html('Desea eliminar el cursos/serminario ' + currentReference.name + '?');
  $frmRemoveReference.attr('action', '/reference/' + id + '/delete');
});

$frmAddReference.form({
  inline: true,
  fields: {
    name: {
      identifier: 'name',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese el nombre'
        }
      ]
    },
    url: {
      identifier: 'url',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese la URL.'
        }
      ]
    },
    kind: {
      identifier: 'kind',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese el tipo.'
        }
      ]
    },
    icon: {
      identifier: 'icon',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese el icono.'
        }
      ]
    }
  }
});
