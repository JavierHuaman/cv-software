
var $skills = JSON.parse(document.querySelector('[name=skills]').value),
    $skillsContainer = $('#skills-container'),
    $frmAddSkill = $('#frm-add-skill'),
    $frmRemoveSkill = $('#frm-remove-skill'),
    $modalAddSkill = $('#modal-add-skill'),
    $modalRemoveSkill = $('#modal-remove-skill'),
    $btnAddSkill = $('#btn-add-skill');

$modalAddSkill
  .modal({
    onApprove : function() {
      $frmAddSkill.submit();
      return false;
    },
    observeChanges : true
  })
  .modal('attach events', ('#' + $btnAddSkill.attr('id')) , 'show');

$modalRemoveSkill
  .modal({
    onApprove : function() {
      $frmRemoveSkill.submit();
    }
  })
  .modal('attach events', '#skills-container .remove-skill' , 'show');

$btnAddSkill.click(function() {
  $frmAddSkill.form('reset');
  $frmAddSkill.form('clear');
  $frmAddSkill.attr('action', '/skill/create');
  $modalAddSkill.find('.header').html('Agregar Conocimiento Informatica');
});

$skillsContainer.find('.edit-skill').click(function() {
  $frmAddSkill.form('reset');
  $frmAddSkill.form('clear');
  var id = $(this).attr('data-id');
  var currentSkill = $skills.find( function(skill) { return skill._id == id } );

  $frmAddSkill.find('[name=header]').val(currentSkill.header);
  $frmAddSkill.find('[name=descriptions]').html(currentSkill.descriptions);
  $frmAddSkill.attr('action', '/skill/' + id + '/update');
  $modalAddSkill.find('.header').html('Editar Conocimiento Informatica : ' + currentSkill.header );
  $modalAddSkill.modal('show');
});

$skillsContainer.find('.remove-skill').click(function() {
  var id = $(this).attr('data-id');
  var currentSkill = $skills.find( function(skill) { return skill._id == id } );

  $modalRemoveSkill.find('.header').html('Desea eliminar el estudio ' + currentSkill.header + '?');
  $frmRemoveSkill.attr('action', '/skill/' + id + '/delete');
});

$frmAddSkill.form({
  inline: true,
  fields: {
    header: {
      identifier: 'header',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese la cabecera.'
        }
      ]
    },
    descriptions: {
      identifier: 'descriptions',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese las descripciones.'
        }
      ]
    }
  }
});

