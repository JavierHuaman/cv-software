
var $works = JSON.parse(document.querySelector('[name=works]').value),
    $worksContainer = $('#works-container'),
    $frmAddWork = $('#frm-add-work'),
    $frmRemoveWork = $('#frm-remove-work'),
    $modalAddWork = $('#modal-add-work'),
    $modalRemoveWork = $('#modal-remove-work'),
    $btnAddWork = $('#btn-add-work'),
    $btnAddDescriptions = $modalAddWork.find('#btn-add-descriptions'),
    $containerDescriptions = $modalAddWork.find('#container-descriptions'),
    $currentWork = null;

$modalAddWork
  .modal({
    onVisible : function() {
      $containerDescriptions.empty();
      if ($currentWork != null) {
        for (var _ii in $currentWork.description ) {
          addDescription($currentWork.description[_ii]);
        }
      }
    },
    onApprove : function() {
      $frmAddWork.submit();
      return false;
    },
    observeChanges : true
  })
  .modal('attach events', ('#' + $btnAddWork.attr('id')) , 'show');

$modalRemoveWork
  .modal({
    onApprove : function() {
      $frmRemoveWork.submit();
    }
  })
  .modal('attach events', '#works-container .remove-work' , 'show');

$btnAddWork.click(function() {
  $frmAddWork.form('reset');
  $frmAddWork.form('clear');
  $frmAddWork.attr('action', '/work/create');
  $currentWork = null;
  $containerDescriptions.empty();
  $modalAddWork.find('.general-header').html('Agregar Experiencia Laboral');
});

$worksContainer.find('.edit-work').click(function() {
  $frmAddWork.form('reset');
  $frmAddWork.form('clear');
  var name = $(this).attr('data-name');

  $currentWork = $works.find( function(work) { return work.name == name } );

  // TODO
  for (var _kk in $currentWork) {
    $frmAddWork.find('[name=' + _kk + ']').val($currentWork[_kk].toString().replace('T00:00:00.000Z', ''));
  }
  $frmAddWork.attr('action', '/work/' + $currentWork.name + '/update');
  $modalAddWork.find('.general-header').html('Editar Experiencia Laboral : ' + $currentWork.name);
  $modalAddWork.modal('show');
});


$worksContainer.find('.remove-work').click(function() {
  var name = $(this).attr('data-name');
  $modalRemoveWork.find('.header').html('Desea eliminar el trabajo ' + name);
  $frmRemoveWork.attr('action', '/work/' + name + '/delete');
});


function addDescription(description) {
  description = description || {};
  var container = $('<div class="two fields" />');
  var general = $('<div class="field" />')
                      .html($('<label />').html('General : '))
                      .append($('<textarea rows="4" name="generals" />').html(description.general));
  var technologies = $('<div class="field" />')
                        .append($('<a class="ui tiny circular red remove-description label" />').html('X'))
                        .append($('<label />').html('Tecnologias : '))
                        .append($('<textarea rows="4" name="technologies" />').html(description.technologies));

  $containerDescriptions.append(container.append(general).append(technologies));
}

$btnAddDescriptions.click(function() {
  addDescription();
});

$containerDescriptions.on('click', '.remove-description', function() {
  var containerField = $(this).parent().parent();
  containerField.remove();
});

$frmAddWork.form({
  inline: true,
  fields: {
    name: {
      identifier: 'name',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese el nombre'
        }
      ]
    },
    charge: {
      identifier: 'charge',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese el cargo'
        }
      ]
    },
    start: {
      identifier: 'start',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese la fecha de inicio'
        }
      ]
    },
    end: {
      identifier: 'end',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese la fecha final'
        }
      ]
    }
  }
});
