
var $studies = JSON.parse(document.querySelector('[name=studies]').value),
    $studiesContainer = $('#studies-container'),
    $frmAddStudy = $('#frm-add-study'),
    $frmRemoveStudy = $('#frm-remove-study'),
    $modalAddStudy = $('#modal-add-study'),
    $modalRemoveStudy = $('#modal-remove-study'),
    $btnAddStudy = $('#btn-add-study');

$modalAddStudy
  .modal({
    onApprove : function() {
      $frmAddStudy.submit();
      return false;
    },
    observeChanges : true
  })
  .modal('attach events', ('#' + $btnAddStudy.attr('id')) , 'show');

$modalRemoveStudy
  .modal({
    onApprove : function() {
      $frmRemoveStudy.submit();
    }
  })
  .modal('attach events', '#studies-container .remove-study' , 'show');

$btnAddStudy.click(function() {
  $frmAddStudy[0].reset();
  $frmAddStudy.attr('action', '/study/create');
  $modalAddStudy.find('.header').html('Agregar Estudios Realizados');
});

$studiesContainer.find('.edit-study').click(function() {
  var center = $(this).attr('data-center');

  var currentStudy = $studies.find( function(study) { return study.center == center } );

  $frmAddStudy.find('[name=center]').val(currentStudy.center);
  $frmAddStudy.find('[name=start]').val(currentStudy.start.replace('T00:00:00.000Z', ''));
  $frmAddStudy.find('[name=end]').val(currentStudy.end.replace('T00:00:00.000Z', ''));
  $frmAddStudy.find('[name=description]').html(currentStudy.description);
  $frmAddStudy.attr('action', '/study/' + currentStudy.center + '/update');
  $modalAddStudy.find('.header').html('Editar Estudio Realizado : ' + currentStudy.center );
  $modalAddStudy.modal('show');
});

$studiesContainer.find('.remove-study').click(function() {
  var center = $(this).attr('data-center');
  $modalRemoveStudy.find('.header').html('Desea eliminar el estudio ' + center + '?');
  $frmRemoveStudy.attr('action', '/study/' + center + '/delete');
});

$frmAddStudy.form({
  inline: true,
  fields: {
    center: {
      identifier: 'center',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese el centro de estudios.'
        }
      ]
    },
    description: {
      identifier: 'description',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese la descripcion.'
        }
      ]
    },
    start: {
      identifier: 'start',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese la fecha inicio.'
        }
      ]
    },
    end: {
      identifier: 'end',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese la fecha de fin.'
        }
      ]
    }
  }
});
