
var $awards = JSON.parse(document.querySelector('[name=awards]').value),
    $awardsContainer = $('#awards-container'),
    $frmAddAward = $('#frm-add-award'),
    $frmRemoveAward = $('#frm-remove-award'),
    $modalAddAward = $('#modal-add-award'),
    $modalRemoveAward = $('#modal-remove-award'),
    $btnAddAward = $('#btn-add-award');

$modalAddAward
  .modal({
    onApprove : function() {
      $frmAddAward.submit();
      return false;
    },
    observeChanges : true
  })
  .modal('attach events', ('#' + $btnAddAward.attr('id')) , 'show');

$modalRemoveAward
  .modal({
    onApprove : function() {
      $frmRemoveAward.submit();
    }
  })
  .modal('attach events', '#awards-container .remove-award' , 'show');

$btnAddAward.click(function() {
  $frmAddAward.form('reset');
  $frmAddAward.form('clear');
  $frmAddAward.attr('action', '/award/create');
  $modalAddAward.find('.header').html('Agregar Estudios Realizados');
});

$awardsContainer.find('.edit-award').click(function() {
  $frmAddAward.form('reset');
  $frmAddAward.form('clear');
  var id = $(this).attr('data-id');
  var currentAward = $awards.find( function(award) { return award._id == id } );

  $frmAddAward.find('[name=name]').val(currentAward.name);
  $frmAddAward.find('[name=center]').val(currentAward.center);
  $frmAddAward.find('[name=year]').val(currentAward.year.replace('T00:00:00.000Z', ''));
  $frmAddAward.attr('action', '/award/' + id + '/update');
  $modalAddAward.find('.header').html('Editar Cursos: ' + currentAward.name );
  $modalAddAward.modal('show');
});

$awardsContainer.find('.remove-award').click(function() {
  var id = $(this).attr('data-id');
  var currentAward = $awards.find( function(award) { return award._id == id } );

  $modalRemoveAward.find('.header').html('Desea eliminar el cursos/serminario ' + currentAward.name + '?');
  $frmRemoveAward.attr('action', '/award/' + id + '/delete');
});

$frmAddAward.form({
  inline: true,
  fields: {
    name: {
      identifier: 'name',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese el nombre'
        }
      ]
    },
    center: {
      identifier: 'center',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese el centro de estudios.'
        }
      ]
    },
    year: {
      identifier: 'year',
      rules: [
        {
          type   : 'empty',
          prompt : 'Ingrese el año realizado.'
        }
      ]
    }
  }
});
